#!/usr/bin/env groovy

node {

    properties([
        parameters([
            choice(choices: 'dev\nprod', description: 'Environment settings: dev or prod', name: 'environment'),
            booleanParam(defaultValue: false, description: 'Update the image in the Stack?', name: 'update'),
            stringParam(defaultValue: 'dashboard', description: 'What is the stack name where this service is deployed?', name: 'stackName'),
        ])
    ])

    checkout scm

    stage('Build local & test') {
        sh 'chmod +x ./gradlew'
        try {
            sh './gradlew clean build'
        }
        finally {
            junit 'build/test-results/**/*.xml'
        }
    }

    def image

    stage('Build Dockerfile') {
        echo "building DockerFile with name: upplication/fenix"
        image = docker.build 'upplication/fenix'
    }

    stage('Test container') {
        // we need to recovery our image
        image.withRun("-e 'SPRING_PROFILES_ACTIVE=dev'") {
            sh 'echo "Tests passed"'
        }
    }

    def projectVersion = sh(script: '''./gradlew properties -q | grep "version:" | awk \'{print $2}\'''', returnStdout: true).trim()

    stage('Push image') {
        docker.withRegistry('https://162801792251.dkr.ecr.eu-west-1.amazonaws.com', 'ecr:eu-west-1:aws-credentials') {
            image.push(projectVersion)
            image.push(params.environment)
            image.push("latest")
        }

        echo " => Removing LOCAL exited containers & unused images"
        sh 'docker rm -v $(docker ps -a -qf status=exited) || echo "No Docker Container to remove"'
        sh 'docker rmi $(docker images -qf dangling=true) || echo "No Docker Image to remove"'
    }

    if (params.update) {

        stage("Update the service at $params.environment") {

            // get portainer user & password
            withCredentials([usernamePassword(credentialsId: 'portainer', usernameVariable: 'usernamePortainer', passwordVariable: 'passwordPortainer')]) {
                usernamePortainer = env.usernamePortainer
                passwordPortainer = env.passwordPortainer

                echo " => Login in Portainer"
                def portainerUrl = 'http://portainer.upplication.' +  "${params.environment == 'dev' ? 'io' : 'com'}:9000"

                // check if swarm exists and can login!
                def authRequestJSON = """{ "Username": "${env.usernamePortainer}", "Password": "${env.passwordPortainer}"}"""
                def response = runCommand("curl --connect-timeout 30 -s -X POST ${portainerUrl}/api/auth -H accept:application/json -H Content-Type:application/json -d '${authRequestJSON}'")

                if (response.exitCode != 0) {
                    println "${portainerUrl} is not running!\nDetailed response =>\n${response}"
                    currentBuild.result = 'SUCCESS'
                    return
                }

                def authJSON = parseJSON(response.stdout)
                def endpointId = parseJSON(sh (script: "curl -s -X GET ${portainerUrl}/api/endpoints -H accept:application/json -H Content-Type:application/json -H 'Authorization: Bearer ${authJSON.jwt}'", returnStdout: true))[0].Id

                echo " => Generate the base64 with user & password & serverAddress to allow the service to download the image from the remote private repository ECR"
                // pull the image
                // easy option (but depends in aws-cli):
                def passwordECR = sh(script: "aws ecr get-login | awk -F \" \" '{print \$6}'", returnStdout: true).trim()
                def registriesJSON = parseJSON(sh(script: "curl -s -X GET ${portainerUrl}/api/registries -H accept:application/json -H Content-Type:application/json -H 'Authorization: Bearer ${authJSON.jwt}'", returnStdout: true))

                def registryId = ''
                for (def registryJSON : registriesJSON) {
                    if (registryJSON.Name == 'ECR-162801792251') {
                        registryId = registryJSON.Id
                        break
                    }
                }

                if (registryId == ''){
                    echo "Registry for AWS ECR not found in ${portainerUrl}"
                    currentBuild.result = 'SUCCESS'
                    return
                }

                def registryJSON = parseJSON(sh(script: "curl -s -X GET ${portainerUrl}/api/registries/${registryId} -H accept:application/json -H Content-Type:application/json -H 'Authorization: Bearer ${authJSON.jwt}'", returnStdout: true))
                // FIXME: registryJSON doesnt return the password
                def authEcrRequestJson = """{ "username": "${registryJSON.Username}", "password": "${passwordECR}", "serveraddress": "${registryJSON.URL}"}"""
                // check login:
                // def authEcrResponseJson = parseJSON(sh(script:"curl -s -X POST ${portainerUrl}/api/endpoints/1/docker/auth -H 'Authorization: Bearer ${authJSON.jwt}' -d '${authEcrRequestJson}'", returnStdout: true))
                // println authEcrResponseJson

                def authEcrToken = sh(script: "echo -n '$authEcrRequestJson' | base64 -w0", returnStdout: true)

                echo " => Update the service: fenix in the stack ${params.stackName}-${params.environment}"
                def imageDocker = "162801792251.dkr.ecr.eu-west-1.amazonaws.com/upplication/fenix:${projectVersion}"

                // update the service
                def serviceName = "${params.stackName}-${params.environment}_fenix"
                def queryFilter = """filters={"name":{"${serviceName}": true}}"""
                def servicesJSON = parseJSON(sh(script: "curl -s -G -X GET ${portainerUrl}/api/endpoints/${endpointId}/docker/services -H accept:application/json -H Content-Type:application/json -H 'Authorization: Bearer ${authJSON.jwt}' --data-urlencode '${queryFilter}'", returnStdout: true))

                for (def serviceJSON : servicesJSON) {
                    if (serviceJSON.Spec.Name == serviceName) {
                        def requestBody = serviceJSON.Spec
                        requestBody.TaskTemplate.ContainerSpec.Image = imageDocker.toString()
                        requestBody = requestBody.toString()
                        sh "curl -s -X POST ${portainerUrl}/api/endpoints/${endpointId}/docker/services/${serviceJSON.ID}/update?version=${serviceJSON.Version.Index} -H accept:application/json -H Content-Type:application/json -H 'Authorization: Bearer ${authJSON.jwt}' -H 'X-Registry-Auth:${authEcrToken}' -d '${requestBody}'"
                    }
                }
            }
        }
    }
}

def runCommand(script) {
    echo "[runCommand:script] ${script}"

    def stdoutFile = "rc.${BUILD_NUMBER}.out"
    script = script + " > " + stdoutFile

    def res = [:]
    res.exitCode = sh(returnStatus: true, script: script)
    res.stdout = sh(returnStdout: true, script: "cat " + stdoutFile)

    sh(returnStatus: true, script: "rm -f " + stdoutFile)

    echo "[runCommand:response] ${res}"
    return res
}

def parseJSON(String text) {
    return readJSON(text: text)
}
