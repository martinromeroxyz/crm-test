package com.upplication.crm.stats;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StatsController {

    private static final String APLICATECA_INDEX = "stats/index";

    @Autowired
    private StatsManager statsManager;

    @RequestMapping(value = "stats")
    public String sales(Model model,
                        @RequestParam(value = "start", required = false)  @DateTimeFormat(pattern = "yyyy-MM-dd") java.util.Date start,
                        @RequestParam(value = "end", required = false)  @DateTimeFormat(pattern = "yyyy-MM-dd") java.util.Date end,
                        @RequestParam(value = "filterType", required = false, defaultValue = "ALL")  FilterType filterType) {

        model.addAttribute("start", start);
        model.addAttribute("end", end);
        model.addAttribute("filterType", filterType);

        List<StatsSales> news = statsManager.getSubscriptions(start, end, filterType);

        List<StatsSales> downs = statsManager.getUnsubscriptions(start, end, filterType);

        model.addAttribute("news", news);
        model.addAttribute("downs", downs);

        return APLICATECA_INDEX;
    }
    
    //FIXME: use custom view resolver
    @RequestMapping(value = "stats/export.csv", method = RequestMethod.POST)
    @ResponseBody
    public String statsExportCSV(@Valid @ModelAttribute StatsAdminForm adminForm, 
    		HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=" + LocalDate.now().toString() + ".csv");
        
        StringBuilder stringBuilder = new StringBuilder();
        
        List<StatsSales> results = new ArrayList<>();
        
        if("news".equals(adminForm.getSelection())) {
        	results = statsManager.getSubscriptions(adminForm.getStart(), adminForm.getEnd(), adminForm.getFilterType());
        } else if("downs".equals(adminForm.getSelection())) {
        	results = statsManager.getUnsubscriptions(adminForm.getStart(), adminForm.getEnd(), adminForm.getFilterType());
        } else {
        	results = Collections.emptyList();
        }
        
        if(Objects.nonNull(results) && !results.isEmpty()) {
        	
        	// title
        	stringBuilder.append("news".equals(adminForm.getSelection()) ? "ALTAS\n" : "BAJAS\n	");
        	
        	// csv header
        	stringBuilder.append("Pricing plan,");
        	
        	List<java.time.LocalDate> dates = results.get(0).getValues().entrySet().stream()
        			.map(Map.Entry::getKey).collect(Collectors.toList());
        	
        	for(java.time.LocalDate date : dates) {
        		String formattedDate = date.format(DateTimeFormatter.ofPattern("MM-yyyy"));
        		stringBuilder.append("," + formattedDate);
        	}
        	stringBuilder.append("\n");
        	
        	// csv content
        	for (StatsSales result : results) {
        		stringBuilder.append(result.getPricingName() + ","); 		

        		String v = result.getValues().entrySet().stream().map(Map.Entry::getValue)
        				.map(String::valueOf).collect(Collectors.joining(","));
        		
        		stringBuilder.append(v + "\n");
            }
        }
        return stringBuilder.toString();
    }
}
