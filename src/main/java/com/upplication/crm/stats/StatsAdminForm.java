package com.upplication.crm.stats;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class StatsAdminForm {
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date start;
	 
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date end;
	 
	FilterType filterType;
	
	String selection;

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public FilterType getFilterType() {
		return filterType;
	}

	public void setFilterType(FilterType filterType) {
		this.filterType = filterType;
	}

	public String getSelection() {
		return selection;
	}

	public void setSelection(String selection) {
		this.selection = selection;
	}

}
