package com.upplication.crm.market.requests.domain;


import es.upplication.entities.type.ScreenshotDeviceType;

import java.io.Serializable;

public class ResourceResponse implements Serializable {

    private int id;
    private int position;
    private ScreenshotDeviceType device;
    private String url;

    public ResourceResponse() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public ScreenshotDeviceType getDevice() {
        return device;
    }

    public void setDevice(ScreenshotDeviceType device) {
        this.device = device;
    }

    @Override
    public String toString() {
        return "ViewResponse{" +
                ", id='" + id + '\'' +
                ", position='" + position + '\'' +
                ", device='" + device + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
