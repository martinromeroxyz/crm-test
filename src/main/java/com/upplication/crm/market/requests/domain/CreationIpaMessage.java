package com.upplication.crm.market.requests.domain;


import java.util.List;

public class CreationIpaMessage {

    private int childUppId;
    private String bundleId;
    private String name;
    private int version;
    private String iconUrl;
    private int requestId;
    private boolean splashWithAds;
    // custom extra plugins
    private List<String> pluginsExtra;
    // apple credentials
    private String username;
    private String password;
    private String teamName;
    private String teamId;
    // apple creation ipa fields fields
    private String mobileProvision;
    private String certificate;
    private String passwordCertificate;
    private String typeCertificate;
    // apple Authentication Key
    private String authKeyId;
    private String authKeyContent;
    // last error, if any
    private String errorMsg;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getChildUppId() {
        return childUppId;
    }

    public void setChildUppId(int childUppId) {
        this.childUppId = childUppId;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public boolean isSplashWithAds() {
        return splashWithAds;
    }

    public void setSplashWithAds(boolean splashWithAds) {
        this.splashWithAds = splashWithAds;
    }

    public String getMobileProvision() {
        return mobileProvision;
    }

    public void setMobileProvision(String mobileProvision) {
        this.mobileProvision = mobileProvision;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getPasswordCertificate() {
        return passwordCertificate;
    }

    public void setPasswordCertificate(String passwordCertificate) {
        this.passwordCertificate = passwordCertificate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getTypeCertificate() {
        return typeCertificate;
    }

    public void setTypeCertificate(String typeCertificate) {
        this.typeCertificate = typeCertificate;
    }

    public List<String> getPluginsExtra() {
        return pluginsExtra;
    }

    public void setPluginsExtra(List<String> pluginsExtra) {
        this.pluginsExtra = pluginsExtra;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setAuthKeyId(String authKeyId) {
        this.authKeyId = authKeyId;
    }

    public String getAuthKeyId() {
        return authKeyId;
    }

    public void setAuthKeyContent(String authKeyContent) {
        this.authKeyContent = authKeyContent;
    }

    public String getAuthKeyContent() {
        return authKeyContent;
    }
}
