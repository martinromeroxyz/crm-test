package com.upplication.crm.market.requests.queue.ios;

import com.rabbitmq.client.Channel;
import com.upplication.crm.market.requests.RequestsManager;
import com.upplication.crm.market.requests.domain.ItunesMessageResponse;
import es.upplication.entities.type.StatusPublishRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

@Component
public class PublishRequestListener {

    private final RequestsManager requestsManager;

    private static final Logger logger = LoggerFactory.getLogger(PublishRequestListener.class);

    public PublishRequestListener(final RequestsManager requestsManager) {
        this.requestsManager = requestsManager;
    }

    /**
     * Method to listen the Itunes Status Queue and set the specific status at the DB
     *
     * @param request PublishRequestResponse response all the information with the worker result
     */
    @RabbitListener(queues = "#{'${rabbitmq.itunes.publishStatus.queue}'}")
    public void receiveMessage(final ItunesMessageResponse request, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws Exception {
        logger.info("***********************************************");
        logger.info("Received message Itunes Status: " + request.toString());
        logger.info("***********************************************");
        int requestId = request.getId();

        if (request.isSuccess()) {
            requestsManager.changeIosStatus(requestId, StatusPublishRequest.IN_EDITION);
            // only receive the market url at the first creation
            if (request.getMarketUrl() != null) {
                requestsManager.setIosMarketUrl(request.getBundleId(), request.getMarketUrl());
            }
            // remove possible previous errors
            requestsManager.setIpaErrorMessage(requestId, null);
        } else {
            requestsManager.changeIosStatus(requestId, StatusPublishRequest.INCORRECT);
            String completeErrorMsg = "Fallo en el paso: "+request.getStepError()+"\nERROR MSG: "+request.getErrorMsg();
            logger.warn("Error process at the Itunes Worker building Request ID: "+requestId+" Step: ", completeErrorMsg);
            requestsManager.setIpaErrorMessage(requestId, completeErrorMsg);
        }

        logger.info("Sending ACK to Itunes Status Queue");
        channel.basicAck(tag, false);
    }

}
