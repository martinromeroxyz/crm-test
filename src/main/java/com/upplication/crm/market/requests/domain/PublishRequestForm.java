package com.upplication.crm.market.requests.domain;

import es.upplication.entities.type.StatusPublishRequest;
import org.hibernate.validator.constraints.Email;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Date;


public class PublishRequestForm {

    @NotNull
    @Min(1)
    private int requestId;

    @NotNull
    @Min(1)
    private int app;

    @NotNull
    private String name;

    @NotNull
    @Pattern(regexp = "^(android|ios)$")
    private String os;

    @NotNull
    private String androidBundleId;

    @NotNull
    private String iosBundleId;

    @NotNull
    private String description;

    @NotNull
    private String language;

    @NotNull
    private String keywords;

    private String videoPath;

    private String changes;

    @NotNull
    private String content;

    @NotNull
    private String category;

    @NotNull
    @Pattern(regexp = "^https?://(.*)\\..*$")
    private String icon;

    private Date deleteDate;

    private int iosVersion;

    private String androidVersion;

    private String ipaUrl;

    private String apkUrl;

    private String iosMarketUrl;

    private String androidMarketUrl;

    private StatusPublishRequest iosStatus;

    private StatusPublishRequest androidStatus;

    private String supportUrl;

    private String copyright;

    // Contact Info
    private String contactName;

    private String contactSurname;

    private String contactPhone;

    @Email
    private String contactEmail;

    private String ipaErrorMessage;

    private boolean editAppleStoreAccount;
    // Apple Store Account fields
    private String appleUsername;
    private String applePassword;
    private String appleTeamId;
    private String appleTeamName;
    private String appleCompanyName;
    // Apple application specific password (application loader)
    private String appleApplicationPassword;
    // apple sign
    private String appleSignCertificateUrl;
    private MultipartFile appleSignCertificate;
    private String appleSignCertificatePass;
    private boolean appleSignCertDevelopment;
    private String appleSignProvisionUrl;
    private MultipartFile appleSignMobileProvision;
    // apple push
    private String applePushAuthKeyId;
    private String applePushAuthKeyContent;

    // Google Play Account fields
    private boolean editGooglePlayAccount;
    private String googleEmail;
    private String googleTeamName;
    private String googleSignKeyAlias;
    private String googleSignPasswordAlias;
    private String googleSignPasswordStore;
    private String googleSignStoreUri;
    private boolean debugAndroid;

    // flag to control if ipa is generated or not
    private boolean generateIpaBuild;

    // development cordova:
    private String pluginsExtra;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getApp() {
        return app;
    }

    public void setApp(int app) {
        this.app = app;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getChanges() {
        return changes;
    }

    public void setChanges(String changes) {
        this.changes = changes;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getIpaUrl() {
        return ipaUrl;
    }

    public void setIpaUrl(String ipaUrl) {
        this.ipaUrl = ipaUrl;
    }

    public String getApkUrl() {
        return apkUrl;
    }

    public void setApkUrl(String apkUrl) {
        this.apkUrl = apkUrl;
    }

    public String getSupportUrl() {
        return supportUrl;
    }

    public void setSupportUrl(String supportUrl) {
        this.supportUrl = supportUrl;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactSurname() {
        return contactSurname;
    }

    public void setContactSurname(String contactSurname) {
        this.contactSurname = contactSurname;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getIpaErrorMessage() {
        return ipaErrorMessage;
    }

    public void setIpaErrorMessage(String ipaErrorMessage) {
        this.ipaErrorMessage = ipaErrorMessage;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public StatusPublishRequest getIosStatus() {
        return iosStatus;
    }

    public void setIosStatus(StatusPublishRequest iosStatus) {
        this.iosStatus = iosStatus;
    }

    public StatusPublishRequest getAndroidStatus() {
        return androidStatus;
    }

    public void setAndroidStatus(StatusPublishRequest androidStatus) {
        this.androidStatus = androidStatus;
    }

    public String getIosMarketUrl() {
        return iosMarketUrl;
    }

    public void setIosMarketUrl(String iosMarketUrl) {
        this.iosMarketUrl = iosMarketUrl;
    }

    public String getAndroidMarketUrl() {
        return androidMarketUrl;
    }

    public void setAndroidMarketUrl(String androidMarketUrl) {
        this.androidMarketUrl = androidMarketUrl;
    }

    public String getAndroidBundleId() {
        return androidBundleId;
    }

    public void setAndroidBundleId(String androidBundleId) {
        this.androidBundleId = androidBundleId;
    }

    public String getIosBundleId() {
        return iosBundleId;
    }

    public void setIosBundleId(String iosBundleId) {
        this.iosBundleId = iosBundleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppleUsername() {
        return appleUsername;
    }

    public void setAppleUsername(String appleUsername) {
        this.appleUsername = appleUsername;
    }

    public String getApplePassword() {
        return applePassword;
    }

    public void setApplePassword(String applePassword) {
        this.applePassword = applePassword;
    }

    public String getAppleTeamId() {
        return appleTeamId;
    }

    public void setAppleTeamId(String appleTeamId) {
        this.appleTeamId = appleTeamId;
    }

    public boolean isDebugAndroid() {
        return debugAndroid;
    }

    public void setDebugAndroid(boolean debugAndroid) {
        this.debugAndroid = debugAndroid;
    }

    public String getGoogleSignKeyAlias() {
        return googleSignKeyAlias;
    }

    public void setGoogleSignKeyAlias(String googleSignKeyAlias) {
        this.googleSignKeyAlias = googleSignKeyAlias;
    }

    public String getGoogleSignPasswordAlias() {
        return googleSignPasswordAlias;
    }

    public void setGoogleSignPasswordAlias(String googleSignPasswordAlias) {
        this.googleSignPasswordAlias = googleSignPasswordAlias;
    }

    public String getGoogleSignPasswordStore() {
        return googleSignPasswordStore;
    }

    public void setGoogleSignPasswordStore(String googleSignPasswordStore) {
        this.googleSignPasswordStore = googleSignPasswordStore;
    }

    public String getGoogleSignStoreUri() {
        return googleSignStoreUri;
    }

    public void setGoogleSignStoreUri(String googleSignStoreUri) {
        this.googleSignStoreUri = googleSignStoreUri;
    }

    public boolean isGenerateIpaBuild() {
        return generateIpaBuild;
    }

    public void setGenerateIpaBuild(boolean generateIpaBuild) {
        this.generateIpaBuild = generateIpaBuild;
    }

    public int getIosVersion() {
        return iosVersion;
    }

    public void setIosVersion(int iosVersion) {
        this.iosVersion = iosVersion;
    }

    public String getAppleSignProvisionUrl() {
        return appleSignProvisionUrl;
    }

    public void setAppleSignProvisionUrl(String appleSignProvisionUrl) {
        this.appleSignProvisionUrl = appleSignProvisionUrl;
    }

    public boolean isEditAppleStoreAccount() {
        return editAppleStoreAccount;
    }

    public void setEditAppleStoreAccount(boolean editAppleStoreAccount) {
        this.editAppleStoreAccount = editAppleStoreAccount;
    }

    public String getAppleCompanyName() {
        return appleCompanyName;
    }

    public void setAppleCompanyName(String appleCompanyName) {
        this.appleCompanyName = appleCompanyName;
    }

    public String getAppleTeamName() {
        return appleTeamName;
    }

    public void setAppleTeamName(String appleTeamName) {
        this.appleTeamName = appleTeamName;
    }

    public MultipartFile getAppleSignMobileProvision() {
        return appleSignMobileProvision;
    }

    public void setAppleSignMobileProvision(MultipartFile appleSignMobileProvision) {
        this.appleSignMobileProvision = appleSignMobileProvision;
    }

    public String getPluginsExtra() {
        return pluginsExtra;
    }

    public void setPluginsExtra(String pluginsExtra) {
        this.pluginsExtra = pluginsExtra;
    }

    public String getApplePushAuthKeyId() {
        return applePushAuthKeyId;
    }

    public void setApplePushAuthKeyId(String applePushAuthKeyId) {
        this.applePushAuthKeyId = applePushAuthKeyId;
    }

    public String getApplePushAuthKeyContent() {
        return applePushAuthKeyContent;
    }

    public void setApplePushAuthKeyContent(String applePushAuthKeyContent) {
        this.applePushAuthKeyContent = applePushAuthKeyContent;
    }

    public boolean isAppleSignCertDevelopment() {
        return appleSignCertDevelopment;
    }

    public void setAppleSignCertDevelopment(boolean appleSignCertDevelopment) {
        this.appleSignCertDevelopment = appleSignCertDevelopment;
    }

    public String getGoogleEmail() {
        return googleEmail;
    }

    public void setGoogleEmail(String googleEmail) {
        this.googleEmail = googleEmail;
    }

    public String getGoogleTeamName() {
        return googleTeamName;
    }

    public void setGoogleTeamName(String googleTeamName) {
        this.googleTeamName = googleTeamName;
    }

    public boolean isEditGooglePlayAccount() {
        return editGooglePlayAccount;
    }

    public void setEditGooglePlayAccount(boolean editGooglePlayAccount) {
        this.editGooglePlayAccount = editGooglePlayAccount;
    }

    public String getAppleSignCertificateUrl() {
        return appleSignCertificateUrl;
    }

    public void setAppleSignCertificateUrl(String appleSignCertificateUrl) {
        this.appleSignCertificateUrl = appleSignCertificateUrl;
    }

    public MultipartFile getAppleSignCertificate() {
        return appleSignCertificate;
    }

    public void setAppleSignCertificate(MultipartFile appleSignCertificate) {
        this.appleSignCertificate = appleSignCertificate;
    }

    public String getAppleSignCertificatePass() {
        return appleSignCertificatePass;
    }

    public void setAppleSignCertificatePass(String appleSignCertificatePass) {
        this.appleSignCertificatePass = appleSignCertificatePass;
    }

    public String getAppleApplicationPassword() {
        return appleApplicationPassword;
    }

    public void setAppleApplicationPassword(String appleApplicationPassword) {
        this.appleApplicationPassword = appleApplicationPassword;
    }
}
