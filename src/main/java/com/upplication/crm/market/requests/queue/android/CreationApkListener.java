package com.upplication.crm.market.requests.queue.android;

import com.rabbitmq.client.Channel;
import com.upplication.crm.market.requests.domain.RequestApkMessage;
import com.upplication.crm.market.requests.util.CreationStatusUtil;
import es.upplication.entities.type.StatusRequest;
import es.upplication.type.OperatingSystemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;


@Component
public class CreationApkListener {

    Logger logger = LoggerFactory.getLogger(CreationApkListener.class);

    final private CreationStatusUtil creationStatusUtil;

    public CreationApkListener(CreationStatusUtil creationStatusUtil) {
        this.creationStatusUtil = creationStatusUtil;
    }

    /**
     * Method to listen the Creation APK Status Queue and set the specific status at the DB
     *
     * @param request PublishRequestResponse response all the information with the worker result
     */
    @RabbitListener(queues = "#{'${rabbitmq.apk.creationStatus.queue}'}")
    public void receiveCreationApkStatusMessage(final RequestApkMessage request, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws Exception {
        logger.info("Received message Creation APK Status for ChildUpp ID " + request.getChildUppId() + ", Request ID: " + request.getRequestId() + " Bundle: "+request.getBundleId());

        int childUppId = request.getChildUppId();

        if (request.isSuccess()) {
            // update status
            creationStatusUtil.updateOSAppInDB(childUppId, OperatingSystemType.ANDROID, request.getApkUrl(), request.getBundleId(), request.getVersion(), request.getIcon(), StatusRequest.SOLVED);
        } else {
            logger.error("Something wrong happens creating the APK with ChildUpp ID: " + request.getChildUppId() + " with Error " + request.getErrorMsg());
            creationStatusUtil.updateChildUppStatusRequest(childUppId, OperatingSystemType.ANDROID, StatusRequest.ERROR);
        }

        logger.info("Sending ACK to Creation APK Status Queue");
        channel.basicAck(tag, false);
    }

}
