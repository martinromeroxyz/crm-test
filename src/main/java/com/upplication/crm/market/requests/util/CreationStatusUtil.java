package com.upplication.crm.market.requests.util;

import com.upplication.crm.market.requests.domain.ItunesMessageRequest;
import com.upplication.crm.market.requests.queue.QueueSender;
import com.upplication.crm.market.requests.queue.ios.CreationIpaListener;
import com.upplication.crm.repository.*;
import es.upplication.entities.*;
import es.upplication.entities.type.DistributionType;
import es.upplication.entities.type.StatusRequest;
import es.upplication.type.OperatingSystemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Calendar;
import static com.upplication.crm.repository.ChildUppOsSpecification.filterByChildUppIdAndOs;
import static com.upplication.crm.repository.OSSpecification.filterByName;
import static org.springframework.data.jpa.domain.Specifications.where;

@Component
public class CreationStatusUtil {

    Logger logger = LoggerFactory.getLogger(CreationIpaListener.class);

    private final ChildUppRepository childUppRepository;

    private final ChildUppOsRepository childUppOsRepository;

    private final OSRepository osRepository;

    @Autowired
    private MarketRequestRepository marketRequestRepository;

    private final BuilderMessageUtil builderMessageUtil;

    private final QueueSender queueSender;

    public CreationStatusUtil(final ChildUppOsRepository childUppOsRepository, final OSRepository osRepository,
                              final BuilderMessageUtil builderMessageUtil, final QueueSender queueSender,
                              final ChildUppRepository childUppRepository) {
        this.childUppRepository = childUppRepository;
        this.childUppOsRepository = childUppOsRepository;
        this.osRepository = osRepository;
        this.builderMessageUtil = builderMessageUtil;
        this.queueSender = queueSender;
    }

    /**
     * Updates the info about the childUpp and adds a new OS with the URL
     *
     * @param childUppId ChildUpp identifier
     * @param url      String
     * @param pathIco  String
     * @param bundleId String mandatory for ios
     * @param version  int mandatory
     * @param status   StatusRequest
     */
    public void updateOSAppInDB(int childUppId, OperatingSystemType osType, String url, String bundleId,
                                 int version, String pathIco, StatusRequest status) {
        ChildUpp childUpp = childUppRepository.findOne(childUppId);
        ChildUppOS childUppOS = childUppOsRepository.findOne(where(filterByChildUppIdAndOs(childUppId, osType.getValue())));

        if (childUppOS == null) {
            OS osChildUpp = osRepository.findOne(where(filterByName(osType.getName())));

            childUppOS = new ChildUppOS();
            childUppOS.setChildUpp(childUpp);
            childUppOS.setOS(osChildUpp);

            childUppOS.setPathIco(pathIco);
            childUppOS.setVersion(version);
            childUppOS.setUrl(url);
            childUppOS.setFriendly_url(url);
            childUppOS.setStatus(status);

            childUppOS.setBundleId(bundleId);

            childUppOS.setDistributionType(DistributionType.STORE);

            childUppOS.setModifyDate(Calendar.getInstance().getTime());

        } else {
            childUppOS.setBundleId(bundleId);

            childUppOS.setDistributionType(DistributionType.STORE);

            childUppOS.setPathIco(pathIco);
            childUppOS.setVersion(version);
            childUppOS.setUrl(url);
            childUppOS.setFriendly_url(url);
            childUppOS.setStatus(status);

            childUppOS.setModifyDate(Calendar.getInstance().getTime());

        }
        childUppOsRepository.saveAndFlush(childUppOS);
    }

    /**
     * Update ChildUpp Status for specific Operating System
     *
     * @param childUppId ChildUpp identifier
     * @param osType Operating System Type
     * @param status New status to be stored
     */
    public void updateChildUppStatusRequest(int childUppId, OperatingSystemType osType, StatusRequest status) {
        ChildUpp childUpp = childUppRepository.findOne(childUppId);
        ChildUppOS childUppOS = childUppOsRepository.findOne(where(filterByChildUppIdAndOs(childUpp.getId(), osType.getValue())));
        if (childUppOS == null) {
            // FIXME: this flow cause a null pointer when the sources try to update or change the childUpp name.
            // FIXME: study if it's not better to not save anything or change the code below to update and change name operations
            OS osChildUpp = osRepository.findOne(where(filterByName(osType.getName())));

            childUppOS = new ChildUppOS();
            childUppOS.setChildUpp(childUpp);
            childUppOS.setOS(osChildUpp);

            childUppOS.setVersion(0);
            childUppOS.setStatus(status);
            childUppOS.setModifyDate(Calendar.getInstance().getTime());

        } else {
            childUppOS.setStatus(status);
            childUppOS.setModifyDate(Calendar.getInstance().getTime());
        }
        childUppOsRepository.saveAndFlush(childUppOS);
    }

    /**
     * Send message for Itunes Publish Exchange to continue the publication
     *
     * @param requestId Publish Request Identifier
     * @param bundleId Bundle Identifier for the APP
     * @param ipaUrl Url received from Creation IPA Worker
     */
    public void sendItunesConnectPublication(int requestId, String bundleId, String ipaUrl) {
        logger.info("Sending to ItunesPublish queue Request ID: "+requestId);
        PublishRequest request = marketRequestRepository.findOne(requestId);
        ItunesMessageRequest message = builderMessageUtil.buildItunesMessageToSend(request, bundleId, ipaUrl);
        queueSender.sendMessage(message);
    }

}
