package com.upplication.crm.market.requests.domain;


public class RequestIpaMessage {

    private boolean success;
    private String ipaUrl;
    private int childUppId;
    private int requestId;
    private String bundleId;
    private int version;
    private String icon;
    private String errorMsg;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getChildUppId() {
        return childUppId;
    }

    public void setChildUppId(int childUppId) {
        this.childUppId = childUppId;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getIpaUrl() {
        return ipaUrl;
    }

    public void setIpaUrl(String ipaUrl) {
        this.ipaUrl = ipaUrl;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
