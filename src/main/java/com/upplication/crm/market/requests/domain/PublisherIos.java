package com.upplication.crm.market.requests.domain;


public class PublisherIos {

    private String username;
    private String password;
    private String teamId;
    private String teamName;
    private String companyName;
    private String applicationPassword;
    private String certificateUrl;
    private String certificatePassword;
    private String provisionUrl;
    private String authKeyId;
    private String authKeyContent;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getCertificateUrl() {
        return certificateUrl;
    }

    public void setCertificateUrl(String certificateUrl) {
        this.certificateUrl = certificateUrl;
    }

    public String getProvisionUrl() {
        return provisionUrl;
    }

    public void setProvisionUrl(String provisionUrl) {
        this.provisionUrl = provisionUrl;
    }

    public String getCertificatePassword() {
        return certificatePassword;
    }

    public void setCertificatePassword(String certificatePassword) {
        this.certificatePassword = certificatePassword;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAuthKeyId() {
        return authKeyId;
    }

    public void setAuthKeyId(String authKeyId) {
        this.authKeyId = authKeyId;
    }

    public String getAuthKeyContent() {
        return authKeyContent;
    }

    public void setAuthKeyContent(String authKeyContent) {
        this.authKeyContent = authKeyContent;
    }

    public String getApplicationPassword() {
        return applicationPassword;
    }

    public void setApplicationPassword(String applicationPassword) {
        this.applicationPassword = applicationPassword;
    }
}
