package com.upplication.crm.market.requests.domain;


import es.upplication.entities.type.ScreenshotDeviceType;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ScreenshotForm {

    @NotNull
    @Min(1)
    private int requestId;

    @NotNull
    @Min(1)
    private int position;

    @NotNull
    private ScreenshotDeviceType device;

    private MultipartFile file;

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public ScreenshotDeviceType getDevice() {
        return device;
    }

    public void setDevice(ScreenshotDeviceType device) {
        this.device = device;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
