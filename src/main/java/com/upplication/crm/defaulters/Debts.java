package com.upplication.crm.defaulters;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Debts {

    private List<DebtLine> debts;

    public List<DebtLine> getDebts() {
        return debts;
    }

    public void setDebts(List<DebtLine> debts) {
        this.debts = debts;
    }
    
}
