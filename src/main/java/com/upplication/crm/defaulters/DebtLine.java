package com.upplication.crm.defaulters;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import es.upplication.entities.PaymentOrderType;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DebtLine {

    private int childUpp;

    private double amount;

    private Date billingDate;

    private PaymentOrderType type;

    public int getChildUpp() {
        return childUpp;
    }

    public void setChildUpp(int childUpp) {
        this.childUpp = childUpp;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getBillingDate() {
        return billingDate;
    }

    public void setBillingDate(Date billingDate) {
        this.billingDate = billingDate;
    }

    public PaymentOrderType getType() {
        return type;
    }

    public void setType(PaymentOrderType type) {
        this.type = type;
    }

}
