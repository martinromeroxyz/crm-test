package com.upplication.crm.defaulters;

import es.upplication.entities.PaymentOrderType;

import java.util.Date;

public class DefaulterLine {

    private String creator;
    private String email;
    private int childUppId;
    private String childUppName;
    private Date date;
    private double quantity;
    private String status;
    private PaymentOrderType type;

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getChildUppId() {
        return childUppId;
    }

    public void setChildUppId(int childUppId) {
        this.childUppId = childUppId;
    }

    public String getChildUppName() {
        return childUppName;
    }

    public void setChildUppName(String childUppName) {
        this.childUppName = childUppName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PaymentOrderType getType() {
        return type;
    }

    public void setType(PaymentOrderType type) {
        this.type = type;
    }
}
