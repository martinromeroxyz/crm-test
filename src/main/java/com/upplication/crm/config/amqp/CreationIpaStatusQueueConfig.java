package com.upplication.crm.config.amqp;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class CreationIpaStatusQueueConfig {

    // Params for Apple Developer Creation IPA Environment Queue
    @Autowired
    private Environment environment;

    @Bean
    public TopicExchange creationIpaStatusExchange() {
        return new TopicExchange(environment.getProperty("rabbitmq.ipa.creationStatus.exchange"));
    }

    @Bean
    public Queue creationIpaStatusQueue() { return new Queue(environment.getProperty("rabbitmq.ipa.creationStatus.queue"), true); }

    @Bean
    public Binding creationIpaStatusDeclareBinding() {
        return BindingBuilder.bind(creationIpaStatusQueue()).to(creationIpaStatusExchange()).with(environment.getProperty("rabbitmq.ipa.creationStatus.routingKey"));
    }

}
