package com.upplication.crm.config.amqp;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ItunesQueueConfig {

    // Params for Itunes Publish Environment
    @Autowired
    private Environment environment;

    @Bean
    public TopicExchange itunesExchange() {
		return new TopicExchange(environment.getProperty("rabbitmq.itunes.publish.exchange"));
	}

    @Bean
    public Queue itunesQueue() { return new Queue(environment.getProperty("rabbitmq.itunes.publish.queue"), true); }

    @Bean
    public Binding itunesDeclareBinding() {
		return BindingBuilder.bind(itunesQueue()).to(itunesExchange()).with(environment.getProperty("rabbitmq.itunes.publish.routingKey"));
	}

}
