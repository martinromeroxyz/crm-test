package com.upplication.crm.config.amqp;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class CreationApkQueueConfig {

    // Params for Creation APK Environment
    @Autowired
    private Environment environment;

    @Bean
    public TopicExchange creationApkExchange() {
        return new TopicExchange(environment.getProperty("rabbitmq.apk.creation.exchange"));
    }

    @Bean
    public Queue creationApkQueue() { return new Queue(environment.getProperty("rabbitmq.apk.creation.queue"), true); }

    @Bean
    public Binding creationApkDeclareBinding() {
        return BindingBuilder.bind(creationApkQueue()).to(creationApkExchange()).with(environment.getProperty("rabbitmq.apk.creation.routingKey"));
    }

}
