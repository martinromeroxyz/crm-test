package com.upplication.crm.config.amqp;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ChildUppQueueConfig {

    // Params for ChildUpp Queue
    @Autowired
    private Environment environment;

    @Bean
    public Queue childUppInactiveQueue() { return new Queue(environment.getProperty("rabbitmq.childupp.inactive.queue"), true); }

    @Bean
    public TopicExchange childUppInactiveExchange() {
        return new TopicExchange(environment.getProperty("rabbitmq.childupp.inactive.exchange"));
    }

    @Bean
    public Binding childUppInactiveDeclareBinding() {
        return BindingBuilder.bind(childUppInactiveQueue()).to(childUppInactiveExchange()).with(environment.getProperty("rabbitmq.childupp.inactive.routingKey"));
    }

    @Bean
    public Queue childUppRestoreQueue() { return new Queue(environment.getProperty("rabbitmq.childupp.restore.queue"), true); }

    @Bean
    public TopicExchange childUppRestoreExchange() {
        return new TopicExchange(environment.getProperty("rabbitmq.childupp.restore.exchange"));
    }

    @Bean
    public Binding childUppRestoreDeclareBinding() {
        return BindingBuilder.bind(childUppRestoreQueue()).to(childUppRestoreExchange()).with(environment.getProperty("rabbitmq.childupp.restore.routingKey"));
    }

    @Bean
    public Queue pwaQueue() { return new Queue(environment.getProperty("rabbitmq.childupp.new-pwa.queue"), true); }

    @Bean
    public TopicExchange pwaExchange() {
        return new TopicExchange(environment.getProperty("rabbitmq.childupp.new-pwa.exchange"));
    }

    @Bean
    public Binding pwaDeclareBinding() {
        return BindingBuilder.bind(pwaQueue()).to(pwaExchange()).with(environment.getProperty("rabbitmq.childupp.new-pwa.routingKey"));
    }

}
