package com.upplication.crm.config;

import es.upplication.file.FileSystemFactory;
import es.upplication.file.util.AmazonS3Util;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationAwsConfig {

    @Value("${upplication.aws.accessKey}")
    private String accessKey;
    @Value("${upplication.aws.secretKey}")
    private String secretKey;

    @Bean
    public AmazonS3Util amazonS3Util() {
        FileSystemFactory fs = new FileSystemFactory(accessKey, secretKey);
        return new AmazonS3Util(null, fs);
    }
}