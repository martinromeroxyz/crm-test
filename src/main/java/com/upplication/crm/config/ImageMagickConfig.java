package com.upplication.crm.config;

import com.upplication.crm.util.ImageMagickTransformer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ImageMagickConfig {

    @Value("${imagemagick.path}")
    private String imageMagickPath;

    @Value("${imagemagick.quality}")
    private String imageMagickQuality;

    @Bean
    public ImageMagickTransformer imageMagickConfiguration() {
        return new ImageMagickTransformer(imageMagickPath, imageMagickQuality);
    }

}
