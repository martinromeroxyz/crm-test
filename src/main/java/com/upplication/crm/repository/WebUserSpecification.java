package com.upplication.crm.repository;

import es.upplication.entities.*;
import es.upplication.entities.type.CustomerType;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WebUserSpecification {

    public static Specification<WebUser> matchesCriteria(final String email, final String customerId, final CustomerType customerType, final Integer childUppId, final boolean deleted) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (email != null && email.trim().length() > 0) {
                predicates.add(criteriaBuilder.like(root.get("email"), "%" + email + "%"));
            }

            if (customerId != null && customerId.trim().length() > 0) {
                predicates.add(criteriaBuilder.like(root.get("customerId"), "%" + customerId + "%"));
            }

            if (customerType != null){
                predicates.add(criteriaBuilder.equal(root.<CustomerType>get("customerType"), customerType));
            }

            if (childUppId != null) {
                Subquery<ChildUpp> subquery = criteriaQuery.subquery(ChildUpp.class);
                Root<ChildUpp> childUpp = subquery.from(ChildUpp.class);

                subquery
                        .select(childUpp.get("id"))
                        .where(
                                criteriaBuilder.and(
                                        criteriaBuilder.equal(childUpp.get("owner").get("id"), root.get("id")),
                                        criteriaBuilder.equal(childUpp.get("id"), childUppId))
                        );

                predicates.add(criteriaBuilder.exists(subquery));
            }

            Predicate predicateFinal = criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));

            if (deleted) {
                return predicateFinal;
            } else {
                return criteriaBuilder.and(predicateFinal, criteriaBuilder.isNull(root.<Date>get("deleteDate")));
            }

        };
    }

    public static Specification<WebUser> filterByPaymentOrder(final PaymentOrder paymentOrder) {
        return (root, query, cb) -> {
            Join<WebUser, ChildUpp> chilupp = root.join("childUPPs");
            Join<ChildUpp, HistoricFeatures> feature = chilupp.join("features");
            Join<ChildUpp, HistoricFeatures> historicFeature = chilupp.join("historicFeatures", JoinType.LEFT);

            // subquery
            Subquery<Integer> subquery = query.subquery(Integer.class);
            Root<PaymentOrder> innerPaymentOrder = subquery.from(PaymentOrder.class);
            final Join<PaymentOrder, HistoricFeatures> innerFeatures = innerPaymentOrder.join("feature");

            subquery
                    .select(innerPaymentOrder.<Integer>get("id"))
                    .where(
                            cb.and(
                                    cb.equal(innerPaymentOrder.<Integer>get("id"), paymentOrder.getId()),
                                    cb.or(
                                            cb.equal(innerFeatures.get("id"), feature.get("id")),
                                            cb.equal(innerFeatures.get("id"), historicFeature.get("id"))
                                    )
                            )
                    );


            return cb.exists(subquery);
        };
    }

    public static Specification<WebUser> groupByChildUppId() {
        return (personRoot, query, cb) -> {
            // join with childupp
            final Join<PublishRequest, ChildUpp> childUpps = personRoot.join("childUpp");

            // subquery
            Subquery<Integer> subquery = query.subquery(Integer.class);
            Root<PaymentOrder> innerPaymentOrder = subquery.from(PaymentOrder.class);
            final Join<PaymentOrder, HistoricFeatures> innerFeatures = innerPaymentOrder.join("feature");

            subquery
                    .select(innerPaymentOrder.<Integer>get("id"))
                    .where(
                            cb.equal(innerFeatures.get("id"), childUpps.get("id"))
                    );

            return cb.equal(personRoot.<Date>get("requestDate"), subquery);
        };
    }

}
