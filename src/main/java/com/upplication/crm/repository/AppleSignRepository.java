package com.upplication.crm.repository;

import es.upplication.entities.AppleSign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AppleSignRepository extends JpaRepository<AppleSign, Integer>,
        JpaSpecificationExecutor<AppleSign> {
}
