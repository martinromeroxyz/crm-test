package com.upplication.crm.repository;

import es.upplication.entities.ChildUppRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ChildUppRequestRepository extends JpaRepository<ChildUppRequest, Integer>,
    JpaSpecificationExecutor<ChildUppRequest> {

    String query = "SELECT * FROM  childupp_request rq \n" +
                    "    inner join pricing p on p.pricing_id = rq.pricing \n" +
                    "    inner join webuser w on w.id = rq.web_user\n" +
                    "WHERE\n" +
                    "    rq.type = 'ADD'\n" +
                    "    -- not deleted user\n" +
                    "    and (w.deleteDate IS NULL OR w.deleteDate >= :bill_date)\n" +
                    "    -- exclude some test user\n" +
                    "    and w.email NOT LIKE '%@upplication.com%'\n" +
                    "    and w.email NOT LIKE '%arnaix%'\n" +
                    "    and w.email NOT LIKE '%bentue%'\n" +
                    "    and w.email NOT LIKE '%norman%'\n" +
                    "    and w.email NOT LIKE '%@emea.nec.com%'\n" +
                    "    -- aplicateca only\n" +
                    "    and w.customerType = 'APLICATECA'\n" +
                    "    and p.name <> 'aplicateca.tasting' \n" +
                    "    -- not free month: (si tengo un bill date a 01/12 y una app creada el 11/11 si que tengo que cobrarle (a partir de 11/12))\n" +
                    "    -- pero si es plan executive si la app creada el 12/12 tengo que cobrarle desde el 12/12 a 31/12\n" +
                    "    and (DATE_ADD(rq.created, INTERVAL 1 MONTH) < DATE_ADD(:bill_date, INTERVAL 1 MONTH) \n" +
                    "          OR p.name = 'aplicateca.executive') \n" +
                    "    and rq.created < DATE_ADD(:bill_date, INTERVAL 1 MONTH)\n" +
                    // not deleted or deleted this month and deleted this month but almost 30 days active or executive aplicateca
                    "   and not exists (select * from childupp_request r2 where r2.type = 'REMOVE' and r2.childupp_request = rq.id and" +
                    "            r2.resolved IS NOT NULL and r2.resolved < :bill_date and \n" +
                    "           (DATE_ADD(rq.created, INTERVAL 1 MONTH) > r2.resolved and r2.pricing <> 22)) \n" +
                    "    group by rq.id";

    /**
     * Find ChildUppRequests active in the month setted by the billDate parameter
     *
     * @param billDate Date, starting at day 1/month/year
     * @return List ChildUppRequest, never null
     */
    @Query(value = query, nativeQuery = true)
    List<ChildUppRequest> requestAplicateca(@Param("bill_date") Date billDate);
}
