package com.upplication.crm.repository;

import es.upplication.entities.OS;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface OSRepository extends JpaRepository<OS, Integer>,
        JpaSpecificationExecutor<OS> {
}
