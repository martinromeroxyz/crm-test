package com.upplication.crm.repository;

import es.upplication.entities.HistoricFeatures;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface FeaturesRepository extends JpaRepository<HistoricFeatures, Integer>,
        JpaSpecificationExecutor<HistoricFeatures> {
}
