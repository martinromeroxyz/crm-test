package com.upplication.crm.repository;

import es.upplication.entities.ChildUpp;
import es.upplication.entities.HistoricFeatures;
import es.upplication.entities.PublishRequest;
import es.upplication.entities.WebUser;
import es.upplication.entities.type.CustomerType;
import es.upplication.entities.type.HistoricFeaturesType;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A class which is used to create Specification objects which are used
 * to create JPA criteria queries for PublishRequest information.
 */
public class MarketRequestSpecification {

    /**
     * get last published first and group by childUpp id
     *
     * @return
     */
    public static Specification<PublishRequest> groupByChildUppId() {
        return (personRoot, query, cb) -> {
            // join with childupp
            final Join<PublishRequest, ChildUpp> childUpps = personRoot.join("childUpp");

            // subquery
            Subquery<Date> subquery = query.subquery(Date.class);
            Root<PublishRequest> innerPublisRequest = subquery.from(PublishRequest.class);
            final Join<PublishRequest, ChildUpp> innerChildUpps = innerPublisRequest.join("childUpp");

            subquery
                    .select(
                            cb.greatest(innerPublisRequest.<Date>get("requestDate"))
                    )
                    .where(
                            cb.equal(innerChildUpps.get("id"), childUpps.get("id"))
                    );

            return cb.equal(personRoot.<Date>get("requestDate"), subquery);
        };
    }
    
    /**
     * filter by childUpp id
     * @param childUppId if <= 0 then the filter do nothing
     * @return Specification
     */
    public static Specification<PublishRequest> byChildUppId(final int childUppId) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (childUppId > 0) {
                predicates.add(criteriaBuilder.equal(root.<Integer>get("childUpp"), childUppId));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    /**
     * Get Publish request By ID
     *
     * @param id Identifier for the Publish Request (Unique)
     * @return Found Publish Request
     */
    public static Specification<PublishRequest> filterById(final int id) {
        return (publishRequestRoot, query, cb) -> cb.equal(publishRequestRoot.<Integer>get("id"), id);
    }
}
