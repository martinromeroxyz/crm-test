package com.upplication.crm.repository;

import es.upplication.entities.ChildUpp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SubscriptionQueries extends JpaRepository<ChildUpp, Integer> {

    /**
     * Get all subscriptions (new hires) related with childupps accumulated by month
     * @param startDate Date, start date to filter
     * @param endDate Date, end date to filter
     * @return List
     */
    @Query(value =
            "SELECT count(*) as count, DATE_FORMAT(hf.created, '%Y-%m') as date, hf.namePricing from historic_features hf " +
                    "WHERE hf.status <> 'TRIAL' and hf.status <> 'TRIAL_EXPIRED' AND hf.created >= :start_date AND hf.created <= :end_date group by (DATE_FORMAT(hf.created, '%m%Y')), hf.namePricing" , nativeQuery = true)
    List<SubscriptionQuery> getSubscriptions(@Param("start_date") Date startDate, @Param("end_date") Date endDate);

    /**
     * Get all subscriptions (new hires) related with childupp_requests not resolved by childupp accumulated by month
     * @param startDate Date, start date to filter
     * @param endDate Date, end date to filter
     * @return List
     */
    @Query(value= "SELECT count(*) as count, DATE_FORMAT(cr.created, '%Y-%m') as date, p.name as pricingName FROM childupp_request cr " +
            "INNER JOIN pricing p ON p.pricing_id = cr.pricing " +
            "WHERE cr.childupp IS NULL AND cr.type = 'ADD' AND cr.created >= :start_date AND cr.created <= :end_date GROUP BY (DATE_FORMAT(cr.created, '%m%Y')), p.name", nativeQuery = true)
    List<SubscriptionQuery> getSubscriptionsChildUppRequests(@Param("start_date") Date startDate, @Param("end_date") Date endDate);

    /**
     * Get all unsubscriptions (new downs) related with childupp_requests accumulated by month
     * @param startDate Date, start date to filter
     * @param endDate Date, end date to filter
     * @return List
     */
    @Query(value= "SELECT count(*) as count, DATE_FORMAT(cr.created, '%Y-%m') as date, p.name  as pricingName FROM childupp_request cr " +
            "INNER JOIN pricing p ON p.pricing_id = cr.pricing " +
            "WHERE cr.childupp IS NULL AND cr.type = 'REMOVE' AND cr.created >= :start_date AND cr.created <= :end_date GROUP BY (DATE_FORMAT(cr.created, '%m%Y')), p.name", nativeQuery = true)
    List<UnsubscriptionQuery> getUnsubscriptionsChildUppRequests(@Param("start_date") Date startDate, @Param("end_date") Date endDate);

    /**
     * Get all unsubscriptions (new downs) for a pricingPlan caused by a upgrade or downgrade accumulated by month
     * @param startDate Date, start date to filter
     * @param endDate Date, end date to filter
     * @return List
     */
    @Query(value =
            "SELECT count(*) as count, DATE_FORMAT(hf.created, '%Y-%m') as date, hf.namePricing from historic_features hf " +
                    "WHERE hf.status <> 'TRIAL' AND hf.status <> 'TRIAL_EXPIRED' AND hf.TYPE='HISTORY' AND hf.deleted >= :start_date AND hf.deleted <= :end_date group by (DATE_FORMAT(hf.created, '%m%Y')), hf.namePricing" , nativeQuery = true)
    List<UnsubscriptionQuery> getUnsubscriptionsByUpgrade(@Param("start_date") Date startDate, @Param("end_date") Date endDate);

    /**
     * Get all unsubscriptions (new downs) for a pricingPlan caused by a removed childUpp accumulated by month
     * @param startDate Date, start date to filter
     * @param endDate Date, end date to filter
     * @return List
     */
    @Query(value =
            "SELECT count(*) as count,  DATE_FORMAT(c.deleteDate, '%Y-%m') as date, hf.namePricing FROM childupp c " +
                    "INNER JOIN historic_features hf ON hf.childUppId = c.id " +
                    "WHERE hf.TYPE='ACTIVE' AND hf.status <> 'TRIAL' AND hf.status <> 'TRIAL_EXPIRED' " +
                    "   AND c.deleteDate IS NOT NULL AND c.deleteDate >= :start_date AND c.deleteDate <= :end_date group by (DATE_FORMAT(c.deleteDate, '%m%Y')), hf.namePricing" , nativeQuery = true)
    List<UnsubscriptionQuery> getUnsubscriptionsByRemoved(@Param("start_date") Date startDate, @Param("end_date") Date endDate);



}
