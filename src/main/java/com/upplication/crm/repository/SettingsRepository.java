package com.upplication.crm.repository;

import es.upplication.entities.Parameter;
import es.upplication.entities.Settings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingsRepository extends JpaRepository<Settings, Integer>,
        JpaSpecificationExecutor<Settings> {
}
