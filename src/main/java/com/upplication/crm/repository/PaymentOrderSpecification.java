package com.upplication.crm.repository;

import es.upplication.entities.*;
import es.upplication.entities.type.StatusPaymentOrderType;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.Date;

public class PaymentOrderSpecification {

    public static Specification<PaymentOrder> filterByDates(final Date start, final Date end) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.<Date>get("date"), start), criteriaBuilder.lessThanOrEqualTo(root.<Date>get("date"), end));
    }

    public static Specification<PaymentOrder> filterByStatus(final StatusPaymentOrderType status) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.<StatusPaymentOrderType>get("status"), status);
    }

    public static Specification<PaymentOrder> paymentTypeNotNone() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.notEqual(root.<PaymentOrderType>get("type"), PaymentOrderType.NONE);
    }

    public static Specification<PaymentOrder> filterByChildUpp(final int childUppId) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            Join<PaymentOrder, HistoricFeatures> feature = root.join("feature");
            Join<ChildUpp, HistoricFeatures> childupp = feature.join("childUpp");
            return criteriaBuilder.equal(childupp.<Integer>get("id"), childUppId);
        };
    }

    public static Specification<PaymentOrder> fetchFeature() {
        return (root, criteriaQuery, criteriaBuilder) -> {
            root.fetch("feature");
            // TODO: is an stupid operation to only fetch the feature
            return criteriaBuilder.isNotNull(root);
        };
    }

    public static Specification<PaymentOrder> byId(final int id) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            return criteriaBuilder.equal(root.<Integer>get("id"), id);
        };
    }
}