package com.upplication.crm.childupps;

import es.upplication.entities.ChildUpp;
import es.upplication.entities.ChildUppPromotion;
import es.upplication.entities.type.StatusHistoricFeatures;

import java.util.Date;

public class App {

    private int id;
    private String name;
    private boolean hasPromotion;
    private String promoCode;
    private double discount;
    private String pricing;
    private StatusHistoricFeatures status;
    private Date creationDate;
    private Date deleteDate;

    private App() {
    }

    public static App fromEntity(ChildUpp childUpp) {
        ChildUppPromotion promotion = childUpp.getFeatures().getPromotion();
        return new App()
                .setId(childUpp.getId())
                .setCreationDate(childUpp.getCreationDate())
                .setDeleteDate(childUpp.getDeleteDate())
                .setName(childUpp.getName())
                .setHasPromotion(promotion != null)
                .setPromoCode(promotion != null ? promotion.getCode() : null)
                .setDiscount(promotion != null ? promotion.getDiscount().doubleValue() : 0)
                .setStatus(childUpp.getFeatures().getStatus())
                .setPricing(childUpp.getFeatures().getNamePricing());
    }

    public boolean isHasPromotion() {
        return hasPromotion;
    }

    public App setHasPromotion(boolean hasPromotion) {
        this.hasPromotion = hasPromotion;
        return this;
    }

    public int getId() {
        return id;
    }

    public App setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public App setName(String name) {
        this.name = name;
        return this;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public App setPromoCode(String promoCode) {
        this.promoCode = promoCode;
        return this;
    }

    public double getDiscount() {
        return discount;
    }

    public App setDiscount(double discount) {
        this.discount = discount;
        return this;
    }

    public String getPricing() {
        return pricing;
    }

    public App setPricing(String pricing) {
        this.pricing = pricing;
        return this;
    }

    public StatusHistoricFeatures getStatus() {
        return status;
    }

    public App setStatus(StatusHistoricFeatures status) {
        this.status = status;
        return this;
    }

    public App setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public App setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
        return this;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }
}
