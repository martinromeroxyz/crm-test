package com.upplication.crm.admin.login;


public class LoginAdminForm {

    private String webuser;

    private String childupp;

    public String getWebuser() {
        return webuser;
    }

    public void setWebuser(String webuser) {
        this.webuser = webuser;
    }

    public String getChildupp() {
        return childupp;
    }

    public void setChildupp(String childupp) {
        this.childupp = childupp;
    }
}
