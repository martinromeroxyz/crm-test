package com.upplication.crm.admin.login;

import com.upplication.crm.support.web.MessageHelper;
import es.upplication.security.TokenAdminLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class LoginAdminController {

    private static final String LOGIN_ADMIN_VIEW = "admin/login/index";

    @Autowired
    LoginAdminManager loginAdminManager;

	@RequestMapping(value = "admin/login")
	public String login(Model model) {
        model.addAttribute(new LoginAdminForm());
        return LOGIN_ADMIN_VIEW;
    }

    @RequestMapping(value = "admin/login", method = RequestMethod.POST)
    public String signin(@Valid @ModelAttribute LoginAdminForm adminForm, Errors errors, RedirectAttributes ra) {


        if ((adminForm.getChildupp() == null || adminForm.getChildupp().isEmpty()) &&
                (adminForm.getWebuser() == null || adminForm.getWebuser().isEmpty())){
            errors.rejectValue("webuser", "empty");
            return LOGIN_ADMIN_VIEW;
        }

        MessageHelper.addSuccessAttribute(ra, "signup.success");

        String url = loginAdminManager.getSigningUrl(adminForm);
        if (url == null) {
            MessageHelper.addErrorAttribute(ra, "empty");
            return LOGIN_ADMIN_VIEW;
        }

        return "redirect:" + url;
    }
}
