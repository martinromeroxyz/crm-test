package com.upplication.crm.sales;

import com.upplication.crm.repository.*;
import es.upplication.entities.PaymentOrder;
import es.upplication.entities.WebUser;
import es.upplication.entities.type.StatusPaymentOrderType;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.upplication.crm.repository.PaymentOrderSpecification.*;
import static org.springframework.data.jpa.domain.Specifications.where;

@Service
public class SalesManager {

    @Autowired
    private PaymentOrderRepository paymentOrderRepository;

    @Value("${upplication.childupp-api.core-url}")
    private String dashboardApiUrl;

    @Transactional
    public Page<SaleLine> getSales(Pageable pageable, Date start, Date end, Integer childUppId) {
        Specifications<PaymentOrder> filter = filters(start, end, childUppId);
        Page<PaymentOrder> salesPage = paymentOrderRepository.findAll(filter, pageable);
        return new PageImpl<>(populate(salesPage.getContent()), pageable, salesPage.getTotalElements());
    }

    @Transactional
    public List<SaleLine> getSales(Date start, Date end, Integer childUppId) {
        Specifications<PaymentOrder> filter = filters(start, end, childUppId);
        List<PaymentOrder> sales = paymentOrderRepository.findAll(filter);
        return populate(sales);
    }

    private Specifications<PaymentOrder> filters(Date start, Date end, Integer childUppId) {
        Specifications<PaymentOrder> filter = where(filterByStatus(StatusPaymentOrderType.SUCCESS)).and(paymentTypeNotNone());

        if (start != null || end != null){
            start = (start == null ? LocalDate.now().withYear(1900).toDate() : start);
            end = (end == null ? LocalDate.now().plusYears(1000).toDate() : end);
            filter = filter.and(filterByDates(start, end));
        }

        if (childUppId != null) {
            filter = filter.and(filterByChildUpp(childUppId));
        }
        return filter;
    }

    private List<SaleLine> populate(List<PaymentOrder> payments) {
        List<SaleLine> results = new ArrayList<>();
        for (PaymentOrder paymentOrder : payments) {
            // Pageable dont allow fetch and we need fetch to override the @where in the historicFeatures
            PaymentOrder paymentOrderWithFeature = paymentOrderRepository.findOne(where(byId(paymentOrder.getId())).and(fetchFeature()));
            // get day (should be ordered)
            // get owner name
            // get amount
            // get num bills
            WebUser owner = paymentOrderWithFeature.getFeature().getChildUpp().getOwner();
            SaleLine line = new SaleLine();
            line.setDay(paymentOrderWithFeature.getDate());
            line.setStart(paymentOrderWithFeature.getBillingDate());
            line.setEnd(LocalDate.fromDateFields(paymentOrderWithFeature.getBillingDate()).plusDays(30).toDate());
            line.setAmount(paymentOrderWithFeature.getAmount().doubleValue());
            // Pageable dont allow fetch and we need fetch to override the @where in the historicFeatures

            line.setChildUppId(paymentOrderWithFeature.getFeature().getChildUpp().getId());
            line.setChildUppName(paymentOrderWithFeature.getFeature().getChildUpp().getName());
            line.setUserName(owner.getName() != null ? owner.getName() + " " + (owner.getSurname() != null ? owner.getSurname() : "") : "N/A");
            line.setBillNumber(paymentOrderWithFeature.getBillNumber());
            line.setType(paymentOrderWithFeature.getType());
            line.setBillUrlOds(dashboardApiUrl + "billing/bill?transactionId="
                    + paymentOrderWithFeature.getTransactionId() + "&type=ods");
            line.setBillUrlPdf(dashboardApiUrl+ "billing/bill?transactionId="
                    + paymentOrderWithFeature.getTransactionId() + "&type=pdf");

            results.add(line);
        }

        return results;
    }
}
